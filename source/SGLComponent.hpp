/*******************************************************
Copyright (c) 2017 Renode
This software is released under the MIT License.
http://opensource.org/licenses/mit-license.php
*******************************************************/

#ifndef SGL_SGLCOMPONENT_HPP
#define SGL_SGLCOMPONENT_HPP

namespace SGL
{
	class SGLEngine;

	class SGLComponent
	{
	public:
		explicit SGLComponent( SGLEngine *engine );
		virtual ~SGLComponent() = default;
	private:
		SGLEngine *pEngine;
	protected:
		SGLEngine *GetEngine() const;
	};
}

#endif //SGL_SGLCOMPONENT_HPP
