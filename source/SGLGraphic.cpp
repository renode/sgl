/*******************************************************
Copyright (c) 2017 Marmot
This software is released under the MIT License.
http://opensource.org/licenses/mit-license.php
*******************************************************/
#include <iostream>
#include "SGLEngine.hpp"
#include "SGLGraphic.hpp"

namespace SGL
{
	namespace Defaults
	{
		constexpr bool vsync = true;
		constexpr unsigned int fps = 60;
	}

	SGLGraphic::SGLGraphic( SGLEngine* engine ) : 
		SGLComponent( engine ),
		fps( Defaults::fps ),
		frame_rato( 0.0f ),
		frame_count( 0 ),
		frame_time( 0 )
	{
	}

	void SGLGraphic::SetAttribute()
	{
		SDL_GL_SetAttribute( SDL_GL_DOUBLEBUFFER, 1 );

	}

	void SGLGraphic::EnableVsync( bool enable )
	{
		if( enable )
		{
			if( !SDL_GL_SetSwapInterval( -1 ) )
			{
				SDL_GL_SetSwapInterval( 1 );
			}
		}
		else
		{
			SDL_GL_SetSwapInterval( 0 );
		}
	}

	void SGLGraphic::SetFPS( unsigned int fps )
	{
		this->fps = fps;
	}

	unsigned int SGLGraphic::GetFPS() const
	{
		return fps;
	}

	float SGLGraphic::GetFrameRato() const
	{
		return frame_rato;
	}

	bool SGLGraphic::Init()
	{
		sdl_glcontext = SDL_GL_CreateContext( GetEngine()->GetWindow()->GetSDLWindow() );
		if( !sdl_glcontext )
		{
			return false;
		}

		//EnableVsync( Defaults::vsync );
		EnableVsync( false );

		return true;
	}

	bool SGLGraphic::Update()
	{
		if( SDL_GL_GetSwapInterval() == 0 )
		{
			WaitFrame();
		}

		SDL_GL_SwapWindow( GetEngine()->GetWindow()->GetSDLWindow() );
		return true;
	}

	void SGLGraphic::Quit()
	{
		SDL_GL_DeleteContext( sdl_glcontext );
	}

	void SGLGraphic::WaitFrame()
	{
		if( frame_count == 0 )
		{
			frame_time = SDL_GetTicks();
		}

		if( frame_count == fps )
		{
			int t = SDL_GetTicks();
			frame_rato = 1000.f / ( ( t - frame_time ) / ( float )fps );
			frame_count = 0;
			frame_time = t;
		}

		frame_count++;
		int time = SDL_GetTicks() - frame_time;
		int wait = frame_count * 1000 / fps - time;

		if( wait > 0 )
		{
			SDL_Delay( wait );
		}
	}
}
