/*******************************************************
Copyright (c) 2017 Renode
This software is released under the MIT License.
http://opensource.org/licenses/mit-license.php
*******************************************************/

#include "SGLEngine.hpp"
#include "SGLWindow.hpp"

namespace SGL
{

	SGLEngine* SGLEngine::pEngine;

	SGLEngine::SGLEngine() :
		pEvent( new SGLEvent( this ) ),
		pWindow( new SGLWindow( this ) ),
		pGraphic( new SGLGraphic( this ) )
	{
		pEngine = this;
	}

	SGLEngine::~SGLEngine()
	{
		delete pGraphic;
		delete pWindow;
		delete pEvent;
	}

	SGLWindow* SGLEngine::GetWindow()
	{
		return pEngine->pWindow;
	}

	SGLEvent* SGLEngine::GetEvent()
	{
		return pEngine->pEvent;
	}

	SGLGraphic* SGLEngine::GetGraphic()
	{
		return pEngine->pGraphic;
	}

}
