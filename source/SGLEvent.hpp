/*******************************************************
Copyright (c) 2017 Renode
This software is released under the MIT License.
http://opensource.org/licenses/mit-license.php
*******************************************************/

#ifndef SGL_SGLSYSTEM_HPP
#define SGL_SGLSYSTEM_HPP

#include <SDL.h>
#include "SGLComponent.hpp"

namespace SGL
{
	class SGLEngine;

	class SGLEvent : public SGLComponent
	{
	public:
		explicit SGLEvent( SGLEngine* engine );
		virtual ~SGLEvent() = default;
		bool Init();
		bool Update();
		void Quit();
	private:
		SDL_Event sdl_event;
	protected:
	};
}

#endif //SGL_SGLSYSTEM_HPP
