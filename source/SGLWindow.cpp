/*******************************************************
Copyright (c) 2017 Renode
This software is released under the MIT License.
http://opensource.org/licenses/mit-license.php
*******************************************************/

#include "SGLEngine.hpp"
#include "SGLWindow.hpp"

namespace SGL
{
	namespace Defaults
	{
		constexpr static int window_width = 800;
		constexpr static int window_height = 600;
		constexpr char window_title[] = "SGL";
	}

	SGLWindow::SGLWindow( SGLEngine * engine ) :
		SGLComponent( engine )
	{
	}

	bool SGLWindow::Init()
	{
		GetEngine()->GetGraphic()->SetAttribute();

		sdl_window = SDL_CreateWindow( Defaults::window_title, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
			Defaults::window_width, Defaults::window_height, SDL_WINDOW_OPENGL );

		if( !sdl_window ){
			return false;
		}

		return true;
	}

	void SGLWindow::Quit()
	{
		SDL_DestroyWindow( sdl_window );
	}

	std::string SGLWindow::SetTitle(const std::string & title)
	{
		SDL_SetWindowTitle( sdl_window, title.c_str() );
		return title;
	}

	SDL_Window* SGLWindow::GetSDLWindow() const
	{
		return sdl_window;
	}
}
