/*******************************************************
Copyright (c) 2017 Renode
This software is released under the MIT License.
http://opensource.org/licenses/mit-license.php
*******************************************************/

#include <SDL.h>
#include "SGLEngine.hpp"

void SGLMain();

int main( int argc, char* argv[] )
{
	SGL::SGLEngine engine;

	if( engine.GetEvent()->Init() &&
		engine.GetWindow()->Init() &&
		engine.GetGraphic()->Init() )
	{
		SGLMain();
	}

	engine.GetGraphic()->Quit();
	engine.GetWindow()->Quit();
	engine.GetEvent()->Quit();
	
	return 0;
}
