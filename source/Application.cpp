/*******************************************************
Copyright (c) 2017 Renode
This software is released under the MIT License.
http://opensource.org/licenses/mit-license.php
*******************************************************/

#include "SGLEngine.hpp"
#include "Application.hpp"

namespace SGL
{
	namespace Application
	{
		bool Update()
		{
			return ( SGLEngine::GetEvent()->Update() && 
				SGLEngine::GetGraphic()->Update() );
		}
	}
}
