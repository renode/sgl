/*******************************************************
Copyright (c) 2017 Renode
This software is released under the MIT License.
http://opensource.org/licenses/mit-license.php
*******************************************************/

#ifndef SGL_SGLWINDOW_HPP
#define SGL_SGLWINDOW_HPP

#include <string>
#include <SDL.h>
#include "SGLComponent.hpp"

namespace SGL
{
	class SGLEngine;

	class SGLWindow : public SGLComponent
	{
	public:
		explicit SGLWindow( SGLEngine* engine );
		virtual ~SGLWindow() = default;
		bool  Init();
		void Quit();
		std::string SetTitle( const std::string& title );
		SDL_Window* GetSDLWindow() const;
	private:
		SDL_Window *sdl_window;
		unsigned int id;
		std::string title;
		bool enable_window;
	};
}

#endif //SGL_WINDOW_HPP
