/*******************************************************
Copyright (c) 2017 Marmot
This software is released under the MIT License.
http://opensource.org/licenses/mit-license.php
*******************************************************/

#ifndef SGL_SGLGRAPHIC_HPP
#define SGL_SGLGRAPHIC_HPP

#include <SDL.h>
#include <SDL_opengl.h>
#include "SGLComponent.hpp"

namespace SGL
{
	class SGLEngine;

	class SGLGraphic : public SGLComponent
	{
	public:
		explicit SGLGraphic( SGLEngine* engine );
		virtual ~SGLGraphic() = default;
		void SetAttribute();
		void SetFPS( unsigned int fps );
		unsigned int GetFPS() const;
		float GetFrameRato() const;
		void EnableVsync( bool enable );
		bool Init();
		bool Update();
		void Quit();
	private:
		void WaitFrame();
		SDL_GLContext sdl_glcontext;
		SDL_Window* sdl_window;
		unsigned int fps;
		unsigned int frame_count;
		unsigned int frame_time;
		float frame_rato;
	protected:
	};
}

#endif //SGL_SGLGRAPHIC_HPP
