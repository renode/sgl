/*******************************************************
Copyright (c) 2017 Renode
This software is released under the MIT License.
http://opensource.org/licenses/mit-license.php
*******************************************************/

#ifndef SGL_SGLENGINE_HPP
#define SGL_SGLENGINE_HPP

#include "SGLEvent.hpp"
#include "SGLWindow.hpp"
#include "SGLGraphic.hpp"

namespace SGL
{
	class SGLEngine
	{
	public:
		SGLEngine();
		~SGLEngine();
		static SGLWindow* GetWindow();
		static SGLEvent* GetEvent();
		static SGLGraphic* GetGraphic();
	private:
		SGLEvent* pEvent = nullptr;
		SGLWindow* pWindow = nullptr;
		SGLGraphic* pGraphic = nullptr;
		static SGLEngine *pEngine;
	};
}

#endif //SGL_SGLENGINE_HPP
