/*******************************************************
Copyright (c) 2017 Renode
This software is released under the MIT License.
http://opensource.org/licenses/mit-license.php
*******************************************************/

#include <string>
#include "SGLEngine.hpp"
#include "Window.hpp"

namespace SGL
{
	namespace Window
	{
		void SetTitle( const std::string& str )
		{
			SGLEngine::GetWindow()->SetTitle( str );
		}
	}
}
