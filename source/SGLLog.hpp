/*******************************************************
Copyright (c) 2017 Renode
This software is released under the MIT License.
http://opensource.org/licenses/mit-license.php
*******************************************************/

#ifndef SGL_SGLLOG_HPP
#define SGL_SGLLOG_HPP

#include <SDL.h>
#include "SGLComponent.hpp"

namespace SGL
{
	class SGLLog : public SGLComponent
	{
	public:

	private:
	protected:
	};
}

#endif //SGL_SGLLOG_HPP
