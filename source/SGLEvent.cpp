/*******************************************************
Copyright (c) 2017 Renode
This software is released under the MIT License.
http://opensource.org/licenses/mit-license.php
*******************************************************/

#include "SGLEvent.hpp"

namespace SGL
{
	SGLEvent::SGLEvent( SGLEngine * engine ) : 
		SGLComponent( engine ),
		sdl_event( {} )
	{
	}

	bool SGLEvent::Init()
	{
		if( SDL_Init( SDL_INIT_EVERYTHING ) != 0 )
		{
			return false;
		}

		SDL_EventState( SDL_QUIT, SDL_ENABLE );

		SDL_EventState( SDL_APP_TERMINATING, SDL_ENABLE );
		SDL_EventState( SDL_APP_LOWMEMORY, SDL_ENABLE );
		SDL_EventState( SDL_APP_WILLENTERBACKGROUND, SDL_ENABLE );
		SDL_EventState( SDL_APP_DIDENTERBACKGROUND, SDL_ENABLE );
		SDL_EventState( SDL_APP_WILLENTERFOREGROUND, SDL_ENABLE );
		SDL_EventState( SDL_APP_DIDENTERFOREGROUND, SDL_ENABLE );

		SDL_EventState( SDL_WINDOWEVENT, SDL_ENABLE );

		SDL_EventState( SDL_SYSWMEVENT, SDL_ENABLE );

		SDL_EventState( SDL_KEYDOWN, SDL_ENABLE );
		SDL_EventState( SDL_KEYUP, SDL_ENABLE );

		SDL_EventState( SDL_TEXTEDITING, SDL_ENABLE );
		SDL_EventState( SDL_TEXTINPUT, SDL_ENABLE );

		SDL_EventState( SDL_KEYMAPCHANGED, SDL_ENABLE );

		SDL_EventState( SDL_MOUSEMOTION, SDL_ENABLE );
		SDL_EventState( SDL_MOUSEBUTTONDOWN, SDL_ENABLE );
		SDL_EventState( SDL_MOUSEBUTTONUP, SDL_ENABLE );
		SDL_EventState( SDL_MOUSEWHEEL, SDL_ENABLE );

		SDL_EventState( SDL_JOYAXISMOTION, SDL_DISABLE );
		SDL_EventState( SDL_JOYBALLMOTION, SDL_DISABLE );
		SDL_EventState( SDL_JOYHATMOTION, SDL_DISABLE );
		SDL_EventState( SDL_JOYBUTTONDOWN, SDL_DISABLE );
		SDL_EventState( SDL_JOYBUTTONUP, SDL_DISABLE );
		SDL_EventState( SDL_JOYDEVICEADDED, SDL_DISABLE );
		SDL_EventState( SDL_JOYDEVICEREMOVED, SDL_DISABLE );

		SDL_EventState( SDL_CONTROLLERAXISMOTION, SDL_DISABLE );
		SDL_EventState( SDL_CONTROLLERBUTTONDOWN, SDL_DISABLE );
		SDL_EventState( SDL_CONTROLLERBUTTONUP, SDL_DISABLE );
		SDL_EventState( SDL_CONTROLLERDEVICEADDED, SDL_DISABLE );
		SDL_EventState( SDL_CONTROLLERDEVICEREMOVED, SDL_DISABLE );
		SDL_EventState( SDL_CONTROLLERDEVICEREMAPPED, SDL_DISABLE );

		SDL_EventState( SDL_FINGERDOWN, SDL_DISABLE );
		SDL_EventState( SDL_FINGERUP, SDL_DISABLE );
		SDL_EventState( SDL_FINGERMOTION, SDL_DISABLE );

		SDL_EventState( SDL_DOLLARGESTURE, SDL_DISABLE );
		SDL_EventState( SDL_DOLLARRECORD, SDL_DISABLE );
		SDL_EventState( SDL_MULTIGESTURE, SDL_DISABLE );

		SDL_EventState( SDL_CLIPBOARDUPDATE, SDL_ENABLE );

		SDL_EventState( SDL_DROPFILE, SDL_ENABLE );
		SDL_EventState( SDL_DROPTEXT, SDL_ENABLE );
		SDL_EventState( SDL_DROPBEGIN, SDL_ENABLE );
		SDL_EventState( SDL_DROPCOMPLETE, SDL_ENABLE );

		SDL_EventState( SDL_AUDIODEVICEADDED, SDL_DISABLE );
		SDL_EventState( SDL_AUDIODEVICEREMOVED, SDL_DISABLE );

		SDL_EventState( SDL_RENDER_TARGETS_RESET, SDL_ENABLE );
		SDL_EventState( SDL_RENDER_DEVICE_RESET, SDL_ENABLE );

		SDL_EventState( SDL_USEREVENT, SDL_DISABLE );

		return true;
	}

	bool SGLEvent::Update()
	{
		while( SDL_PollEvent( &sdl_event ) )
		{
			switch( sdl_event.type )
			{
			case SDL_QUIT:
				return false;
			case SDL_APP_TERMINATING:
				return false;
			default:
				;
			}
		}
		return true;
	}

	void SGLEvent::Quit()
	{
		SDL_Quit();
	}

}
