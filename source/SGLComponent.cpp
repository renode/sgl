/*******************************************************
Copyright (c) 2017 Renode
This software is released under the MIT License.
http://opensource.org/licenses/mit-license.php
*******************************************************/

#include "SGLComponent.hpp"
#include "SGLEngine.hpp"

namespace SGL
{
	SGLComponent::SGLComponent( SGLEngine * engine ) : 
		pEngine( engine )
	{
	}

	SGLEngine* SGLComponent::GetEngine() const
	{
		return pEngine;
	}
}
