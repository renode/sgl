#!/bin/sh

TAR=/bin/tar
MAKE=/usr/bin/make
CURRENT=`pwd`
TP=$CURRENT/ThirdParty
TARGZ=.tar.gz
SDL2=SDL2-2.0.5
SDL2_image=SDL2_image-2.0.1
SDL2_mixer=SDL2_mixer-2.0.1
SDL2_net=SDL2_net-2.0.1
SDL2_ttf=SDL2_ttf-2.0.14

## You can add to need library
export SDL2_CONFIG=$CURRENT/ThirdParty/SDL2/bin/sdl2-config
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:`find /usr -type f -name 'libGL.*' | sed 's/\/libGL.*//'`

##Check thirdpary files
existCheck() {
	filename=$1
	if [ -e $filename ];then
		echo "$filename is found."
		return 0
	else
		echo "Failed existCheck($filename)"
		exit 1
	fi
}

##Expand thirdpary from source
Vomit() {
	filename=$1
	$TAR xvzf $filename > /dev/null
	if [ $? -eq 0 ];then
		echo "Compleate vomit $filename"
	else
		echo "Failed Vomit($filename)"
		exit 1
	fi	
}

##Configure makefile of thirdpartys
Configure() {
	cd $1
	./configure --prefix=$TP/`echo $1 | cut -d "-" -f 1` > /dev/null
	if [ $? -eq 0 ];then
		echo "Compleate configure $1"
	else
		echo "Failed Configure($1)"
		exit 1
	fi
}

##make && make install
Make() {
	cd $1
	$MAKE > /dev/null
	if [ $? -eq 0 ];then
		echo "Compleate make $1"
	else
		exit 1
	fi
	$MAKE install > /dev/null
	if [ $? -eq 0 ];then
		echo "Compleate install $1"
	else
		echo "Failed Make($1)"
		exit 1
	fi
}

case $1 in
"--check")
	existCheck $TP/$SDL2$TARGZ
	existCheck $TP/$SDL2_image$TARGZ
	existCheck $TP/$SDL2_mixer$TARGZ
	existCheck $TP/$SDL2_net$TARGZ
	existCheck $TP/$SDL2_ttf$TARGZ
	;;
"--expand")	
	Vomit $TP/$SDL2$TARGZ
	Vomit $TP/$SDL2_image$TARGZ
	Vomit $TP/$SDL2_mixer$TARGZ
	Vomit $TP/$SDL2_net$TARGZ
	Vomit $TP/$SDL2_ttf$TARGZ
	;;
"--make")	
	Configure $SDL2
	Make $SDL2
	Configure $SDL2_image
	Make $SDL2_image
	Configure $SDL2_mixer
	Make $SDL2_mixer
	Configure $SDL2_net
	Make $SDL2_net
	Configure $SDL2_ttf
	Make $SDL2_ttf
	;;
"--help")
	echo "./build_thirdparty.sh [OPTION]"
        echo "   *           - Auto build"
	echo "   --check     - Check thridparty files for build."
	echo "   --expand    - Expand thirdparty files"
	echo "   --make      - Make thirdparty"
	echo "   --clean-all - Reset build setup"
	;;
*)
	existCheck $TP/$SDL2$TARGZ
        existCheck $TP/$SDL2_image$TARGZ
        existCheck $TP/$SDL2_mixer$TARGZ
        existCheck $TP/$SDL2_net$TARGZ
        existCheck $TP/$SDL2_ttf$TARGZ
	Vomit $TP/$SDL2$TARGZ
        Vomit $TP/$SDL2_image$TARGZ
        Vomit $TP/$SDL2_mixer$TARGZ
        Vomit $TP/$SDL2_net$TARGZ
        Vomit $TP/$SDL2_ttf$TARGZ
	Configure $SDL2
        Make $SDL2
        Configure $SDL2_image
        Make $SDL2_image
        Configure $SDL2_mixer
        Make $SDL2_mixer
        Configure $SDL2_net
        Make $SDL2_net
        Configure $SDL2_ttf
        Make $SDL2_ttf
	;;
esac

echo "Compleate build of thirdparty."

exit 0
