/*******************************************************
Copyright (c) 2017 Renode
This software is released under the MIT License.
http://opensource.org/licenses/mit-license.php
*******************************************************/

#ifndef SGL_WINDOW_HPP
#define SGL_WINDOW_HPP

namespace SGL
{
	namespace Window
	{
		void SetTitle( const std::string& str );
	}
}

#endif //SGL_WINDOW_HPP
