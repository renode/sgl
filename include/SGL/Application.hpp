/*******************************************************
Copyright (c) 2017 Renode
This software is released under the MIT License.
http://opensource.org/licenses/mit-license.php
*******************************************************/

#ifndef SGL_APPLICATION_HPP
#define SGL_APPLICATION_HPP

namespace SGL
{
	namespace Application
	{
		bool Update();
	}
}

#endif //SGL_APPLICATION_HPP
