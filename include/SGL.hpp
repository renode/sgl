/*******************************************************
Copyright (c) 2017 Renode
This software is released under the MIT License.
http://opensource.org/licenses/mit-license.php
*******************************************************/

#ifndef SGL_SGL_HPP
#define SGL_SGL_HPP

#include "SGL\Application.hpp"
#include "SGL\Window.hpp"

using namespace SGL;

#endif //SGL_SGL_HPP
